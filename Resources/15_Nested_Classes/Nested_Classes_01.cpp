/*
- Why would one use nested classes in C++?
  -> Nested classes for hiding implementation details.
*/
#include<iostream>
using namespace std;
class A {
   private:
   class B {
      private:
      int num;
      public:
      void getdata(int n) {
         num = n;
      }
      void putdata() {
         cout<<"The number is "<<num;
      }
   } b;
   public:
   void display(){b.getdata(10);}
};
int main() {
   cout<<"Nested classes in C++"<< endl;
   //A :: B obj;
   A a;
   a.display();
   //obj.putdata();
   return 0;
}
/* Output *//*
Nested classes in C++
The number is 9
*/
